# PVR Docker Apps Experience

Since State Format V2, PVR has grown some convenience that enables system builders to more easily compose systems made of one BSP and many containers.

To get started you would use ```pvr app-add``` to add a new container to your stystem:

```
$ pvr app-add --from nginx webserver
```

This will generate a ```webserver/``` folder with a matching ```src.json``` for you. It will also automatically run the ```pvr app-install``` command which can also run manually to produce fresh runtime artifacts from the source description:


```
$ pvr app-install webserver
```

Once run you can inspect the produced output and use pvr to commit and post then to one of your devices:

```
# check status of files on disk
$ pvr status
? webserver/lxc.container.conf
? webserver/root.squashfs
? webserver/root.squashfs.docker-digest
? webserver/run.json
? webserver/src.json

# add new files to pvr control
$ pvr add .

# commit new files
$ pvr commit
Adding webserver/lxc.container.conf
Adding webserver/root.squashfs
Adding webserver/root.squashfs.docker-digest
Adding webserver/run.json
Adding webserver/src.json

# post changes to your device
$ pvr post
``` 

To ensure that runtime bits are 100% reproducible we include the concrete docker digest that pvr consumed during app-add/install in the src.json.

In order to get the latest bits by docker name (e.g. for instance if the branch tag has been changed or moved forward) use ```pvr app-update```:

```
# update to latest "nginx:latest"
$ pvr app-update webserver
Application updated

# confirm that therer were changes
$ pvr status
C webserver/src.json

# commit etc.
$ pvr commit
Committing /tmp/ppp/api2-canary-01/.pvr/objectswebserver/src.json
```


