# Clone Your Systems

Pantavisor managed systems automatically will sync their
config and runtime factory state on first contact to their
pantahub instance.

After the sync the system runtime state can be retrieved
through Pantahub endpoints. To make this easy, ```pvr```
offers the ability to ```clone``` the system.

In order to clone a system, copy the PVR Clone URL from
pantahub and use pvr:

```
pvr clone https://pvr.pantahub.com/user1/device1
```

PVR will ask you to log in, download the objects and unpack
them locally on disk.

