# Get the Logs

Getting the logs for your systems happen through the pantahub /logs endpoint.

This endpoints gives you access to any log uploaded by your systems through
the /logs endpoint.

By default Panavisor will itself use such log facilities and all official
Pantahub platforms also use the /logs endpoint to make all relevant logging
information accessible.

The logs endpoint can be monitored in www.pantahub.com device details view
in the "Console" tab

<screenshot>

Or you can use the ```pvr logs``` command to conveniently tail the current
log stream of your devices:

```
pvr logs
```

Other higher level pantahub apps might offer their own view on system logs
so please also consule the documentation of your app.

