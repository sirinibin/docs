For the context of this guide, all instructions refer to the Raspberry Pi 3. This will be extended once more devices are added as official Beta devices.

## Initial setup

In order to try out the Raspberry Pi 3 examples you need to setup your build and testing environment.

### Building with docker

The following packages are needed for building:

```
curl docker
```

You also need [`repo`](https://source.android.com/source/using-repo):

```
$ curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
$ chmod a+x ~/bin/repo
$ export PATH=~/bin/:$PATH
```

### Get the source code

This build system is based on the Alchemy tool, plus modifications specific to the requirements of Pantavisor-enabled images. To initialize it, we use [`repo`](https://source.android.com/source/using-repo):

```
$ repo init -u https://gitlab.com/pantacor/pv-manifest
$ repo sync -j10
```

### Installing PVR

PVR is the remote CLI tool to drive PantaHub operations.

You can find instructions to download pre-built AMD64 and ARMHF binaries, as well as build from source on our [GitLab release page](https://gitlab.com/pantacor/pvr/tags). You can also download the binaries directly from the following links, and place somewhere on your current PATH.

 * amd64: [pvr-006-amd64.tar.gz](https://gitlab.com/pantacor/pvr/uploads/d51b21e1f10fb62357164d5a062924f4/pvr-006-amd64.tar.gz)
 * armhf: [pvr-006-armhf.tar.gz](https://gitlab.com/pantacor/pvr/uploads/c51cf829a94690fdb6282d13bc215389/pvr-006-armv6.tar.gz)

Then we need to register for PantaHub before proceeding with the build:

### Registering user

The first thing you need to do to interact with PantaHub is to register a user account. A user account gives you access to the full API, including the object store, and also grants you access to the dashboard on www.pantahub.com.

Register your user with the following command:

```$ pvr register -u youruser -p yourpassword -e your@email.tld```

This will generate a json response with the server-generated part of the credentials:

```
2017/06/19 11:08:43 Registration Response: {
  "id": "5947949b85188a000c143c2e",
  "type": "USER",
  "email": "your@email.tld",
  "nick": "youruser",
  "prn": "prn:::accounts:/5947949b85188a000c143c2e",
  "password": "yourpassword",
  "time-created": "2017-06-19T09:08:43.767224118Z",
  "time-modified": "2017-06-19T09:08:43.767224118Z"
}
```

You can also go to http://www.pantahub.com and follow the sign-up process on the web interface.

![](ph-welcome.png)

Your account is not ready for use until you have followed email verification, however during the current pre-release period we have disabled automatic verification. Please ping the team for approval of your account and so that the right storage and transfer quotas are set.

## Build All

For building, the flow is very similar to the Android build system, except that targets are not setup in advance of operations but rather these are conditional on the target and the flow is managed by pv-scripts/build.sh.

Current supported targets are ```[malta-qemu, arm-rpi2, arm-rpi3]``` and a few other test ones.

In order to build a target in full all you need is to run the following, where arm-rpi3 can be replaced by any of the supported targets.

By default the build system will generate a default image that has no containers (therefore no functionality, including no rootfs).

In order to build functionality into our example images, we need to use a "remote trail" from PantaHub.com, which is just a reference device definition that the build system uses to extend the default assets. The tools used behind scenes for this process are introduced later, so for now we will just point to the prime example for Raspberry Pi 3.

The prime example for this is our reference (public) **Raspberry Pi 3 Alpine Hotspot trail**, which you can find at:

https://www.pantahub.com/u/examples/devices/5cf52d591dceed0009b713d4/step/0

For the final image the full target has to be called:

```$ PVR_MERGE_SRC=https://pvr.pantahub.com/examples/rpi3-init-device/0 ./build.docker.sh arm-rpi3```

This will generate all dependencies of the arm-rpi3 build tree and build a final image according to the values defined in ```config/rpi3/image.config```. You can find the generated image in ```out/rpi3/rpi3-pv-1024MiB.img```. This is an SD card image.

## Build individual components

You can also build individual components of the system by calling the submodule directly as secondary target:

```BUILD=./build.docker.sh```

This builds the Pantavisor binary and all its dependencies:
```$ $BUILD arm-rpi3 init```

This builds the pv_lxc container runtime plugin for Pantavisor:
```$ $BUILD arm-rpi3 pv_lxc```

This assembles the initrd image '0base.cpio.xz' from all build assets:
```$ $BUILD arm-rpi3 image```

You can also clean individual components by appending ```-clean``` to the build subtarget:
```$ $BUILD arm-rpi3 init-clean```

This runs the 'trail' assembly step, which results in the final PV-trail for storage:
```$ $BUILD arm-rpi3 trail```

## Setup your Raspberry Pi3

To install your RPi3 image, write the image to the SD card you will use. Replace sdX with the correct device node assigned by your kernel. Make sure the device is unmounted before flashing, you can do that with

```
$ sudo umount /dev/sdX*
$ sudo dd if=out/rpi3/rpi3-pv-1024MiB.img of=/dev/sdX bs=32M
$ sync
```

Make sure you run the sync command in order to flush all write buffers to the SD card before removing it to place it in your RPi3.

You are now ready to [boot](#first-boot) your RPi3 with Pantavisor for the first time. Place your SD card in the slot and plug power in.

### First Boot

The default RPi3 image is running a platform based on Alpine Linux, which automatically brings up basic cabled networking and sets up a hotspot with ssid "apboot" for configuration and first boot operations.

The example image provided is running a few more things to help in development and debugging. For example there is a full busybox instance next to Pantavisor, including a telnetd that allows you to introspect the root mount namespace to get more familiar with the system internals.

On first boot Pantavisor will start the Alpine Linux platform and wait for it to configure the network devices until Pantahub.com is reachable. Once this happens, Pantavisor will proceed to register the newly seen device with Pantahub.com and issue a challenge for the device to be claimed by a registered user account.

**IMPORTANT**: It is assumed that the Raspberry Pi is connect to an internet-facing network via the Ethernet port. It is also assumed for all commands that ```192.168.0.1``` is the reachable IP address, however this might be different depending on what you get from your local network.

You can connect to the following network with the following credentials in order to reach the device:

```
ssid: apboot
pass: pantacor
```

You should receive an IP address in the 192.168.0.0/24 range, with the Raspberry Pi 3 getting address 192.168.0.1. You can connect to the device in two ways: on production builds the development environment mentioned above is not enabled, but on this pre-release example there is a telnetd instance running in the context of the initrd.

```
$ telnet 192.168.0.1
#
```

There is also an SSH server (dropbear) running on the Alpine platform which is currently booted on the device. If you log in this way you will be logged into the Alpine container, and therefore you will be unable to access the Pantavisor initrd except for a few control interfaces.
```
$ ssh -lroot 192.168.0.1
password: alpine
```

### Claiming your device

**IMPORTANT**: This section describes how to automatically claim your device using pvr. If you want to do it manually, see how to do it in the [malta-qemu get started guide](get-started-malta.md).

Your first use of `pvr` will be when claiming your device. In order to claim your device you will need a Pantahub account, so make sure you have created one and have had it approved before the following. We are mostly interested now in the following command:

```$ pvr scan```

With this command, we will be able to discover devices in our network, both claimed and unclaimed, if they have no owner. We must begin by discovering and claiming our device:

![](unclaimed-pvr-windows.png)

If `pvr` finds a device on the local network it will present this metadata for you to claim it via the command quoted in the output. Just run that command as-is and your user should now own your device.

It can take a minute or two, depending on your connection, for a device to fully show up on Pantahub's device list the first time it boots. Wait a few if it hasn't showed up, it eventually will. Once your device shows up as claimed and under your ownership, you can interact with it via `clone` and `post` commands. For example, if you `clone` a device you will get a checkout of the different container assets that make up this firmware revision.

## Owning your Raspberry Pi3

Once your Raspberry Pi3 has been booted and claimed with Pantahub, you can control your device. The basic operations you will need for that job are highlighted in this section.

### The Pantavisor checkout

The PVR tool allows you to "clone" the current state of a device. In other words, it lets you get the set of objects (containers, configuration, data blobs) that make up the current running revision of your system.

On our example image, the device only has one revision so far, which is revision 0. This corresponds to the "factory" state, which is basically what has been seeded to the image during build. For an unidentified image, this factory state is not pushed to the cloud until a user account claims the specific device.

To clone the current running revision of our device we can do (replace the ID at the end with the ID obtained before when registering the device):

```
$ pvr clone https://api.pantahub.com/trails/DEVICE-ID pi3-checkout
*** Login (/type [R] to register) @ https://api.pantahub.com/auth (realm=pantahub services) ***
Username: ricmm
Password: *****
8f51d2fa3113dca [OK] Total: 12611584 Bytes at 334154 Bytes/Sec
7a510393bd2a059 [OK] Total: 4841472 Bytes at 167394 Bytes/Sec
932068e5963b1ff [OK] Total: 4812152 Bytes at 163174 Bytes/Sec
0586f4db7e7defb [OK] Total: 5890048 Bytes at 174133 Bytes/Sec
de878a7e0a3b4f2 [OK] Total: 1002 Bytes at 204 Bytes/Sec
f86bd7e55f1d664 [OK] Total: 873988 Bytes at 158350 Bytes/Sec

$ ls pi3-checkout
0base.cpio.xz     alpine-mini.squashfs  kernel.img         modules.squashfs     pantavisor.json
alpine-mini.json  firmware.squashfs     lxc-alpine.config  pantavisor.cpio.xz4
```

This checkout is an exact copy of what is currently on-device, under the storage section given to the revision which is currently running (in this case, revision 0). The relationship between all these files is defined in **pantavisor.json**. This top level file is the one which is parsed by Pantavisor to setup the running "step".

### Pantavisor.json

The pantavisor.json file on our example image looks like this:

```
$ cat pantavisor.json | json
{
  "initrd": "0base.cpio.xz",
  "linux": "kernel.img",
  "platforms:": [
    "alpine-mini"
  ],
  "volumes": [
    "alpine-mini.squashfs",
    "firmware.squashfs",
    "modules.squashfs"
  ],
  "firmware": "/volumes/firmware.squashfs"
}
```

All the paths that can be found in this file are in the context of the root mount namespace (the one available to the PV process). In order to better understand these paths, the mountpoints, and how things work from the initrd, we suggest that you connect via telnet to the telnetd instance running in the initrd. This will give you a full debug shell. You can also find an in-mem log of Pantavisor's own output at ```/pv/logs/pantavisor.log```. There is more information about cloud-backed logging at the end of this guide.

```
$ telnet 192.168.0.1
#
```

### Linux and Initrd

Both these entries serve an obvious purpose: they map the boot assets (initrd and kernel blobs) to bootloader-friendly locations depending on your bootloader's configuration. In this case 0base.cpio.xz is the initrd which contains Pantavisor and the demo helpers, and kernel.img is the RPi3 uImage for u-boot to consume. On device you can see these, and their mappings on:

```
$ telnet 192.168.0.1
# ls -i /storage/trails/0/.pv
     20 pv-initrd.img       19 pv-kernel.img
# ls -i /storage/trails/0/
     18 0base.cpio.xz              21 alpine-mini.squashfs       19 kernel.img                 20 modules.squashfs           31 pantavisor.json
     28 alpine-mini.json           22 firmware.squashfs          16 lxc-alpine.config          17 pantavisor.cpio.xz4
```

### Platforms

A Pantavisor platform is what defines how each container is setup on the running device. In this example there is only one platform **alpine-mini**, and therefore it is the only full-system container which is brought up. This platform is further defined in the JSON file that has it's own name (alpine-mini.json).

```
{
  "configs": [
    "lxc-alpine.config"
  ],
  "exec": "/sbin/init",
  "name": "alpine-mini",
  "share": [
    "NETWORK",
    "UTS",
    "IPC"
  ],
  "type": "lxc"
}
```

This JSON represents the bits that Pantavisor needs to know to correctly bring up the container. The name is what is matched in **pantavisor.json** and the other fields are self-explanatory. We have a pointer to the ```init``` process of the container, the namespaces we would like to share from the root, the list of configs to pass to the runtime and ultimately, the type.

The type defines which container "runtime" we will use. In this current image there is only support for the lxc runtime, provided via the pv_lxc plugin. There is a very simple API through which more runtimes and be extended, but ultimately the plugin is in charge to interpret all the data above and do the correct setup on behalf of Pantavisor's controller.

### Volumes

The volumes list is a way to give arbitrary files on the checkout a mount point on the running system. In this case we ask Pantavisor to mount the set ```modules.squashfs, firmware.squashfs, alpine-mini.squashfs``` so they become available to the running platforms.

Volumes are mounted onto the ```/volumes``` directory and on our example it looks like this:

```
$ telnet 192.168.0.1
# mount
...
/dev/loop0 on /volumes/alpine-mini.squashfs type squashfs (ro,relatime)
/dev/loop1 on /volumes/firmware.squashfs type squashfs (ro,relatime)
/dev/loop2 on /volumes/modules.squashfs type squashfs (ro,relatime)
/dev/loop1 on /lib/firmware type squashfs (ro,relatime)
...
```

If you take a look at the platform configuration file ```lxc-alpine.config``` we can see it uses direct references to the mount point to perform it's own setup operations. Also, in this case, it defines an arbitrary upper mount for an overlayfs which maps directly to the disk storage, instead of a revisioned file. This could also map to another loop mount for the upper, and thus you can have it revisioned as part of the trail operations.

```
$ cat lxc-alpine.config
...
lxc.rootfs = overlayfs:/volumes/alpine-mini.squashfs:/storage/apbootup.disk/upper
...
```

### Firmware

The firmware hint is nothing but a logical indication to Pantavisor as to which mount point contains firmware that must be made available to the platforms for loading. In our case this comes from a squashfs mountpoint, but it might as well be another partition mount, or directly from the initrd, in case we need to load anything to reach the cold storage.

### Driving your device with PVR

The whole point of these tools is the ability to quickly and safely iterating on changes to the platforms, volumes, and full device state with simple and familiar revisioning-system operations.

For example, let's consider our current running device. What if I could take an example share endpoint from somebody else, clone that state and then post it to my device in order to give it a try.

For this example we will work on the existing clone which was done in the above sections. We will modify some files and push back again to the RPi3 and see it walk the new step.

If we haven't cloned before, let's make sure we do so now:

```
$ pvr clone https://api.pantahub.com/trails/DEVICE-ID pi3-checkout
*** Login (/type [R] to register) @ https://api.pantahub.com/auth (realm=pantahub services) ***
Username: ricmm
Password: *****
8f51d2fa3113dca [OK] Total: 12611584 Bytes at 334154 Bytes/Sec
7a510393bd2a059 [OK] Total: 4841472 Bytes at 167394 Bytes/Sec
932068e5963b1ff [OK] Total: 4812152 Bytes at 163174 Bytes/Sec
0586f4db7e7defb [OK] Total: 5890048 Bytes at 174133 Bytes/Sec
de878a7e0a3b4f2 [OK] Total: 1002 Bytes at 204 Bytes/Sec
f86bd7e55f1d664 [OK] Total: 873988 Bytes at 158350 Bytes/Sec
```
You can then do operations on this checked-out example under ```example-clone```, like modify a config file or adapt something to your own needs. Once we have modified files, we just do a simple commit and post as if you were operating on a git tree.

```
$ cd example-clone/
$ vim lxc-alpine.config
$ pvr commit
Committing /tmp/clones/pi3-checkout/.pvr/objects/lxc-alpine.config
$ pvr post https://api.pantahub.com/trails/DEVICE-ID
*** Login (/type [R] to register) @ https://api.pantahub.com/auth (realm=pantahub services) ***
Username: ricmm
Password: *****
Uploaded.
Uploaded.
Uploaded.
Uploaded.
Uploaded.
Uploaded.
Posted JSON: {"id":"DEVICE-ID-2","owner":"prn:pantahub.com:auth:/user1","device":"prn:::devices:/DEVICE-ID","committer":"","trail-id":"DEVICE-ID","rev":2,"commit-msg":"","state":{"#spec":"pantavisor-multi-platform@1","0base.cpio.xz":"7fa9a5ec44e147264e1cab72c262201d23e968498c44ce9cd7311a68cd9b55bf","alpine-mini.json":{"configs":["lxc-alpine.config"],"exec":"/sbin/init","name":"alpine-mini","share":["NETWORK","UTS","IPC"],"type":"lxc"},"alpine-mini.squashfs":"219e14651a6f2158bead0bcf37c9efa7dca2b9a96f3661d9d78e1f7d4118e7a1","firmware.squashfs":"d24632ad262ce01fb9b4104fed33b57a7b38ae93567830441a90fa5699f3a2f7","kernel.img":"fec9b1db203e4ceb3b45d6bf09b6d1c971d9db0e90498b9142ee53c578269497","lxc-alpine.config":"de878a7e0a3b4f23ea5b47520c8105d569f543d76c49ab0b2f6b3a5472cd5162","modules.squashfs":"abe82a1b95c7314355da396ee7a25459aace231ad3057692572d90c6799d432b","pantavisor.json":{"firmware":"/volumes/firmware.squashfs","initrd":"0base.cpio.xz","linux":"kernel.img","platforms:":["alpine-mini"],"volumes":["alpine-mini.squashfs","firmware.squashfs","modules.squashfs"]}},"progress":{"progress":0,"status-msg":"","status":"NEW","log":""},"step-time":"2017-06-19T13:48:06.041920148Z","progress-time":"1970-01-01T00:00:00Z"}
```

We can see how this bumps the revision of the current device to "1" and incorporates a new object ID for the object that refers to lxc-alpine.config. You can also see a full JSON output of what is being saved in the cloud as the device state.

This shows how you can easily share full checkouts between multiple devices, as well as put out "reference" states that can be consumed by other developers and tested out on their devices with a couple very simple operations. In this case we did minimal changes, but it could also be that the new step is entirely different: for example a Raspbian platform, or an OpenWRT platform with other application containers in parallel.

### Monitoring on PantaHub.com: Device dashboard

PantaHub.com provides a fully fledged monitoring interface so that you can interact with your account's own device and object registry. This let's you visualize the current state of each device, how they are walking through available updates as well as doing introspection on the details of each step, including the objects that make it up.

![](ph-list.png)

You can also see your device details directly on PantaHub

![](ph-details.png)

### Logging on Pantahub.com

Pantavisor also exposes a cloud-backed logging feature. This feature is currently only used internally by Pantavisor to commit its internal logs every now and then to the cloud for introspection by the user, but it is also meant for injection of logs by running platforms, either automated or manual.

Logs are currently very aggressive on the default image configurations to put pressure on the backend and smoke out bugs, so the frequency of logs on the backend storage will be very high.

***

## Now... the rest is up to you!
