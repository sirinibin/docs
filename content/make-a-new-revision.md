# Make a new system revision

Once you have the device cloned on disk, you can
make changes to the runtime state on disk and once happy
use pvr commit to stage the current state on disk
as a checkpoint that you can revert to or that you can
upload as a new device revision as one of your devices.

To see current, not-staged file changes, use ```pvr status```
command"

```
pvr status .
```

status output marks file that are not in currently staged
pvr index:

 * A - file is not index, but have been marked as to-be-committed (aka pvr add)
 * C - file is in index, but disk version has changed, pvr commit will update file in index
 * ? - file is not in index and has not yet been added, pvr commit will ignore this file
 * D - file is in index, but disk version does not have this file. pvr commit will remove file from index

To see some json diff output of the json format you can also use ```pvr diff```

```
pvr diff
```

This will display a basic inline diff of json content in the repo as well as a simple hashsum
mismatch diff.

To stage/checkpoint the current changes use pvr commit

```
pvr commit
```

