This guide refers to the setup of the `Raspberry Pi 3 Model B` device for use with the set of demos made for the LimeSDR platform by Lime Microsystems and others. For other models of the Raspberry Pi we recommend you get in touch with us at team@pantahub.com.

## Image Setup

In order to try out the Raspberry Pi 3 Model B examples you need to install our base Pantavisor-enabled image. This image contains a small Alpine Linux based container that provides discovery services and basic connectivity so that our development tools can be used to work on your device. You can download the base image from:

 * Raspberry Pi 3 Model B: [rpi3-pv-1024MiB.img.xz](https://drive.google.com/file/d/1InkJHSBAcD8Ezf2xfvk1vkLxPHbwZ3Y5/view?usp=sharing)

You can install this image with your preferred tool. Specific instructions for your operating system can be found on the Raspberry Pi installation instructions for [Linux](https://www.raspberrypi.org/documentation/installation/installing-images/linux.md) as well as for [Windows](https://www.raspberrypi.org/documentation/installation/installing-images/windows.md)

For quick Linux instructions you can use the `dd` tool following these steps (remember to substitute /dev/sdX for the device node corresponding to your microSD card).

```
unxz rpi3-pv-1024MiB.img.xz
umount /dev/sdX*
sudo dd if=rpi3-pv-1024MiB.img of=/dev/sdX bs=32M
sync
```

## Installing PVR

Our main development tool is called PVR and it lets you interact with your Pantavisor-enabled device remotely through Pantahub. This CLI will be what you use to `clone` your device as well as `post` different demos to it for easy testing and seamless onboarding. You can download it from the following locations:

 * Linux (amd64): [pvr-006-amd64.tar.gz](https://gitlab.com/pantacor/pvr/uploads/d51b21e1f10fb62357164d5a062924f4/pvr-006-amd64.tar.gz)

   To install the Linux version you need to place the extracted binary in your `$PATH` and make sure it is made executable with `chmod +x`.

 * Window (amd64): [pvr-006-win10-x64.zip](https://gitlab.com/pantacor/pvr/uploads/8c2a619bfd568d58f2a24a3b3dead8b2/pvr-006-win10-64.zip)

   To install the Windows version all you need is to place it in a directory to which your user has access and can run executables from. `C:\Users\YOURUSER` is usually a good location.

Once you have it installed just calling the `pvr` command from your shell should show you the help menu, where you can get familiarized with the different features that it provides.

## First Boot

The default RPi3 image is running a platform based on Alpine Linux, which automatically brings up basic cabled networking and sets up a hotspot with ssid "apboot" for configuration and first boot operations.

The example image provided is running a few more things to help in development and debugging. For example there is a full busybox instance next to Pantavisor, including a telnetd that allows you to introspect the root mount namespace to get more familiar with the system internals.

On first boot Pantavisor will start the Alpine Linux platform and wait for it to configure the network devices until Pantahub.com is reachable. Once this happens, Pantavisor will proceed to register the newly seen device with Pantahub.com and issue a challenge for the device to be claimed by a registered user account.

**IMPORTANT**: It is assumed that the Raspberry Pi is connect to an internet-facing network via the Ethernet port. It is also assumed for all commands that ```192.168.0.1``` is the reachable IP address, however this might be different depending on what you get from your local network.

You can connect to the following network with the following credentials in order to reach the device:

```
ssid: apboot
pass: pantacor
```

You should receive an IP address in the 192.168.0.0/24 range, with the Raspberry Pi 3 getting address 192.168.0.1. You can connect to the device in two ways: on production builds the development environment mentioned above is not enabled, but on this pre-release example there is a telnetd instance running in the context of the initrd.

```
$ telnet 192.168.0.1
#
```

There is also an SSH server (dropbear) running on the Alpine platform which is currently booted on the device. If you log in this way you will be logged into the Alpine container, and therefore you will be unable to access the Pantavisor initrd except for a few control interfaces.
```
$ ssh -lroot 192.168.0.1
password: alpine
```

## Claiming your device

**IMPORTANT**: This section describes how to automatically claim your device using pvr. If you want to do it manually, see how to do it in the [malta-qemu get started guide](get-started-malta.md).

Your first use of `pvr` will be when claiming your device. In order to claim your device you will need a Pantahub account, so make sure you have created one and have had it approved before the following. We are mostly interested now in the following command:

```$ pvr scan```

With this command, we will be able to discover devices in our network, both claimed and unclaimed, if they have no owner. We must begin by discovering and claiming our device:

![](unclaimed-pvr-windows.png)

If `pvr` finds a device on the local network it will present this metadata for you to claim it via the command quoted in the output. Just run that command as-is and your user should now own your device.

It can take a minute or two, depending on your connection, for a device to fully show up on Pantahub's device list the first time it boots. Wait a few if it hasn't showed up, it eventually will. Once your device shows up as claimed and under your ownership, you can interact with it via `clone` and `post` commands. For example, if you `clone` a device you will get a checkout of the different container assets that make up this firmware revision.


## Cloning from and Posting to a device

In this example we will be cloning a reference demo device that contains the `limescan` demo project. In the following image we are using the `pvr` command to clone it. If you would like to try the same, you can also do it by running:

```
$ pvr clone https://pvr.pantahub.com/limemicro/limescan-device/4
```

![](pvr-clone-demo-linux.png)

Once you have cloned this device you can proceed to push it to the device that you claimed before. You can find your device nickname by going to Pantahub and seeing it on the device list. For example, if your device is named `picked_skunk` then you can `post` to it in the following way:

```
$ pvr post https://pvr.pantahub.com/ricmm/picked_skunk
```

If you run the above from the previous `limescan-device` check out, you will be sending a full firmware revision that contains the limescan demo, and thus your Raspberry Pi 3 will consume the update, install, reboot and start the new platform containers that make up this demo. You can track this process on [Pantahub](https://www.pantahub.com) of course.

![](post-pvr-linux.png)

## What else?

Now that you have this demo up, why don't you try the others? Check out the following devices on Pantahub that you can use to follow the same process from above, `clone` these guys and then `post` them to your own device to try out the projects!

* [limescan-device](https://www.pantahub.com/u/limemicro/devices/5b9f6ef3b2698100090d337e)
* [limescan-device-daily](https://www.pantahub.com/u/limemicro/devices/5b96d1acb2698100091336fd)
* [lime-dvb-rx](https://www.pantahub.com/u/limemicro/devices/5ba4a254b2698100090e19e9)
* [lime-dvb-tx](https://www.pantahub.com/u/limemicro/devices/5ba4a654b2698100090e1f14)

We will continuously add more and more demos as time goes by, so keep checking here for more. If you have questions, feel free to reach out at team@pantahub.com.
