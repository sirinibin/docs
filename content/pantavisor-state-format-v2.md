# Pantavisor State Format (Spec v2)

Pantavisor State format describes the json format to configure the pantavisor runtime state.

Right now the format supports running pantavisor both in system as well as no-system mode. While the former includes the abilty to manage lifecycle of BSP components such as the linux kernel, modules and firmware as well as pantavisor itsef, the latter allows for easy usage of pantavisor to just run app payloads using our minmimal container runtime.


## Requirements

When drafting this Spec v2, we took the following requirements as input validate whether this new format is suitable for shipping:

1. ability to reproduce from source
2. source format input format compatible with UI drafting
3. install (install) from source
4. customize install from source parameters
5. update to latest from source (refresh/update)
6. ship source alongside runtime
9. ability to change templates for generation
10. directory structure allows for copy paste directory tree to create a new platform

## Spec Details

The results of that drafting process can be found in this "Spec Details" section.

### System: Format (```<system>```)

A complete system is made up of 1 (one) BSP as well as one to many containers. The state format hence allows allows to configure 

```
{
    "#spec": "pantavisor-service-system@1",
  
    "README.md": "xxx",

    "<container1>/src.json": {..},
    "<container1>/run.json": {..},
    "<container1>/file1...": "xxx",

    "bsp/src.json": {..},
    "bsp/run.json": {..},
    "bsp/file1....": {..},
    
    "storage-mapping.json": {}
    "network-mapping.json": {}
}
```

### System: specification ID (```#spec```)

To ensure our toolling and runtime environment can be properly maintained we include an explicit specification identifier that is associated with a specific spec (like this).

The **```#spec```** identifier of this state format is **```"pantavisor-service-system@1"```**.

### Container: Source (```<container>/src.json```)

Each container managed by pantavisor must have a clear definition on what needs running and how. But even more so its convenient if the system has the meta information and configs included that are needed to reproduce running containers from scratch.

```
"<container>/src.json" : {
    "#spec": "service-manifest-src@1",
    "template": "builtin-lxc-docker|<custom-template-name>",
    "config": {
        ... (docker image manifest overwrites)
        "Cmd": "<custom command>"
    },
    "docker_name": "asac/docker",
    "docker_tag": "latest",
    "docker_digest": "sha256:xxxxxxxxxxxxx",
    "persistence": {
        "lxc-overlay": "permanent",
        "/volume1": "revision",
        "/volume2": "revision",
        "/path/to/somewhere": "permanent"
    }
 }
```

#### Name field

Runtime name of container used in execution engine

* Key: ```name```
* Value: ```[A-Za-z0-9-_]```

#### Template field

Template to use for processing the src.json and produce the runtime artifacts out of it.

* Key: ```template```
* Value: ```[builtin-*|template/dir/path/|URL]```


Right now the current only value for builtin templates is:

* ```builtin-lxc-docker```

Template will hold one to many files that each are golang templates. pvr app-install will extract default config map from the source artifact (e.g. in docker case the the container config manifest) and will add elements specified in the ```config``` element parameters to the template processing. 


### Container: Run (```<container>/run.json```)


```
"<container>/run.json" : {
    "#spec": "service-manifest-run@1",
    "type": "lxc",
    "config": "lxc.container.conf"
    "storage" : {
        "lxc-overlay" : {
            "persistence" : "permanent"
        },
        "docker--volume1" : {
            "persistence": "revision"
        },
        "docker--volume2" : {
            "persistence": "boot"
        },
    },
    "root-volume: "root.squashfs",
    "volumes" : [
        "something-else.squashfs"
    ]
},
"<container>/root.squashfs": "xxxx",
"<container>/lxc.container.conf": "xxxx",
"<container>/something-else.squashfs": "xxxx",
```

#### Name field

Runtime name of container used in execution engine

* Key: ```name```
* Value: ```[A-Za-z0-9-_]```

#### Type field

The execution engine to use for running this container. Currently only ```lxc``` is supported.

* Key: ```type```
* Value: ```lxc```

#### Config field

Path to the exection engine runtime config. In case of lxc containers this will be a ```lxc.container.conf``` formatted file.

* Key: ```config```
* Value: ```[path-in-system]```

#### Storage field

Runtime storage information that will allow pantavisor to provision persistent paths with right lifecycle and backing store so that execution engine can use that for mounting.

In case of ```lxc``` a special ```lxc-overlay```

