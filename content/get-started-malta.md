For the context of this guide, all instructions refer to the Malta QEMU emulated board.

## Initial setup

In order to try out the Malta examples you need to setup your build and testing environment.

The following packages are needed for building:

```
bridge-utils curl docker
```

You also need [`repo`](https://source.android.com/source/using-repo):

```
$ curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
$ chmod a+x ~/bin/repo
$ export PATH=~/bin/:$PATH
```

### Get the source code

This build system is based on the Alchemy tool, plus modifications specific to the requirements of Pantavisor-enabled images. To initialize it, we use [`repo`](https://source.android.com/source/using-repo):

```
$ repo init -u https://gitlab.com/pantacor/pv-manifest
$ repo sync -j10
```

### Installing PVR

PVR is the remote CLI tool to drive PantaHub operations.

You can find instructions to download pre-built AMD64 and ARMHF binaries, as well as build from source on our [GitLab release page](https://gitlab.com/pantacor/pvr/tags). You can also download the binaries directly from the following links, and place somewhere on your current PATH.

 * amd64: [pvr-006-amd64.tar.gz](https://gitlab.com/pantacor/pvr/uploads/d51b21e1f10fb62357164d5a062924f4/pvr-006-amd64.tar.gz)
 * armhf: [pvr-006-armhf.tar.gz](https://gitlab.com/pantacor/pvr/uploads/c51cf829a94690fdb6282d13bc215389/pvr-006-armv6.tar.gz)

Then we need to register for PantaHub before proceeding with the build:

### Registering user

The first thing you need to do to interact with PantaHub is to register a user account. A user account gives you access to the full API, including the object store, and also grants you access to the dashboard on www.pantahub.com.

Register your user with the following command:

```$ pvr register -u youruser -p yourpassword -e your@email.tld```

This will generate a json response with the server-generated part of the credentials:

```
2017/06/19 11:08:43 Registration Response: {
  "id": "5947949b85188a000c143c2e",
  "type": "USER",
  "email": "your@email.tld",
  "nick": "youruser",
  "prn": "prn:::accounts:/5947949b85188a000c143c2e",
  "password": "yourpassword",
  "time-created": "2017-06-19T09:08:43.767224118Z",
  "time-modified": "2017-06-19T09:08:43.767224118Z"
}
```

You can also go to http://www.pantahub.com and follow the sign-up process on the web interface:

![](ph-welcome.png)

Your account is not ready for use until you have followed email verification, however during the current pre-release period we have disabled automatic verification. Please ping the team for approval of your account and so that the right storage and transfer quotas are set.

## Build and Use

For building, the flow is very similar to the Android build system, except that targets are not setup in advance of operations but rather these are conditional on the target and the flow is managed by pv-scripts/build.sh.

Current supported targets are ```[malta-qemu, arm-rpi2, arm-rpi3]``` and a few other test ones.

In order to build a target in full all you need is to run the following, where malta-qemu can be replaced by any of the supported targets.

By default the build system will generate a default image that has no containers (therefore no functionality, including no rootfs).

In order to build functionality into our example images, we need to use a "remote trail" from PantaHub.com, which is just a reference device definition that the build system uses to extend the default assets. The tools used behind scenes for this process are introduced later, so for now we will just point to the prime example for Malta.

The prime example for this is our reference (public) **LEDE 17.01.4 trail**, which you can find at:

https://www.pantahub.com/u/examples/devices/5cf627111dceed0009bbc44d/step/0

For the final image the full target has to be called:

```$ PVR_MERGE_SRC=https://pvr.pantahub.com/examples/malta-init-device/0 ./build.docker.sh malta-qemu```

This will generate all dependencies of the malta-qemu build tree and build a final image according to the values defined in ```config/malta/image.config```. You can find the generated image in out/malta/flash-malta-16384.img. This is a pflash QEMU bootable image to be used with the locally built host QEMU.

### Build individual components

You can also build individual components of the system by calling the submodule directly as secondary target:

```$ BUILD=./build.docker.sh```

This builds the Pantavisor binary and all its dependencies:
```$ $BUILD malta-qemu init```

This builds the pv_lxc container runtime plugin for Pantavisor:
```$ $BUILD malta-qemu pv_lxc```

This assembles the initrd image '0base.cpio.xz' from all build assets:
```$ $BUILD malta-qemu image```

You can also clean individual components by appending ```-clean``` to the build subtarget:
```$ $BUILD malta-qemu init-clean```

This runs the 'trail' assembly step, which results in the final PV-trail for storage:
```$ $BUILD malta-qemu trail```

### Use your QEMU Malta example image

In order to run the example image we must use the host.qemu built in the above instructions. A helper script allows for easy configuration of bridge networking and all that you need to quickly bring up a test environment. You can start the image with:

```$ scripts/qemu.sh out/malta/flash-malta-16384.img```

The example image provided is running a few more things to help in development and debugging. For example there is a full busybox instance next to Pantavisor, including a telnetd that allows you to introspect the initrd and root mount namespace to get more familiar with the system internals.

On first boot Pantavisor will start the Malta LEDE platform and wait for it to configure the network devices until PantaHub.com is reachable. Once this happens, Pantavisor will proceed to register the newly seen device with PantaHub.com and issue a challenge for the device to be claimed by a registered user account.

The device should receive an IP address in the ```192.168.53.0/24``` range, but due to default LEDE routing you cannot SSH or telnet (into the debug environment) right away. You first need to drop the relevant routing rules or just flush the whole table for easy access. To do this, you need to execute this command in the qemu.host promt:

```# iptables -F```

If you try to telnet after opening the routing then you will get a simple shell in the initrd environment so that you can introspect the runtime of Pantavisor and how it sets up and administers the containers.

```$ telnet 192.168.53.X```

You can also SSH directly into the running LEDE instance. If you log in this way you will be logged into the container, and therefore you will be unable to access the Pantavisor initrd except for a few control interfaces.

```$ ssh -lroot 192.168.53.X```

As we said before, on first boot Pantavisor will register a temporary device without an owner. This gives you a grace period to claim the device with your user account. Once logged into the device via SSH there are two files that give you the needed information to perform a succesful claim:

```
/pantavisor/device-id
/pantavisor/challenge
```

The values present here will be used with the PVR tool to claim your device, but for that we first need a User Account created in the PantaHub.com registry. Remember the location of these files, and let's go ahead with creating your User Account.

## Claim ownership of your device at pantahub.com

The first thing to do in order to interact with your Raspberry Pi is to claim the device. By claiming the device it's identifier becomes uniquely associated with your user account, and all the objects and relevant trail information become private under your namespace.

On the [first boot](#first-boot) step above we introduced the hint files that Pantavisor exposes to the main platform to exchange some key information. In this case we need the challenge and the device-id of the temporary device so that it can be claimed by the just-registered user account.

```
# cat /pantavisor/challenge
pleasantly-finer-unicorn
# cat /pantavisor/device-id
5b582638c67920b9de2
```

We can then use the PVR tool to claim with the user account. Please note that the last argument is the device endpoint, in full URL form, to the Pantahub host where this device exists.

***IMPORTANT***: Remember to use the device-id and challenge from above, not the example, in all PVR commands.

```
$ pvr claim -c CHALLENGE https://api.pantahub.com/devices/DEVICE-ID
*** Login (/type [R] to register) @ https://api.pantahub.com/auth (realm=pantahub services) ***
Username: youruser
Password: ********
```

You can also claim your device directly from the web interface of PantaHub.com. Once it has been claimed and the device has realized this, it will show up in your devices list.

![](ph-claim.png)

If everything goes well this means your device has been claimed by the user account. You can now use PVR to clone your device, work on it's state and push a new revision.

### The Pantavisor checkout

The PVR tool allows you to "clone" the current state of a device. In other words, it lets you get the set of objects (containers, configuration, data blobs) that make up the current running revision of your system.

On our example image, the device only has one revision so far, which is revision 0. This corresponds to the "factory" state, which is basically what has been seeded to the image during build. For an unidentified image, this factory state is not pushed to the cloud until a user account claims the specific device.

To clone the current running revision of our device we can do (replace the ID at the end with the ID obtained before when registering the device):

```
$ pvr clone https://api.pantahub.com/trails/DEVICE-ID malta-checkout
*** Login (/type [R] to register) @ https://api.pantahub.com/auth (realm=pantahub services) ***
Username: ricmm
Password: *****
ecaec2f8a4d19f4 [OK] Total: 540 Bytes at 2692 Bytes/Sec
85686be7fa1ae51 [OK] Total: 2097152 Bytes at 290200 Bytes/Sec
8beb1a3e0ae743b [OK] Total: 937816 Bytes at 235360 Bytes/Sec
5bf5463be0c6307 [OK] Total: 2089583 Bytes at 324485 Bytes/Sec
d6c10fc92841f60 [OK] Total: 1724416 Bytes at 307185 Bytes/Sec

$ ls malta-checkout/
0base.cpio.xz  lede-17014-os.json lede-17014-malta-be.squashfs  kernel.img
lede-lxc.conf pantavisor.json writable.ext4
```

This checkout is an exact copy of what is currently on-device, under the storage section given to the revision which is currently running (in this case, revision 0). The relationship between all these files is defined in **pantavisor.json**. This top level file is the one which is parsed by Pantavisor to setup the running "step".

### Pantavisor.json

The pantavisor.json file on our example image looks like this:

```
$ cat pantavisor.json | json
{
  "initrd": "0base.cpio.xz",
  "linux": "kernel.img",
  "platforms:": [
    "lede-17014-os"
  ],
  "volumes": [
    "lede-17014-malta-be.squashfs"
  ]
}
```

All the paths that can be found in this file are in the context of the root mount namespace (the one available to the PV process). In order to better understand these paths, the mountpoints, and how things work from the initrd, we suggest that you connect via telnet to the telnetd instance running in the initrd. This will give you a full debug shell. You can also find an in-mem log of Pantavisor's own output at ```/pv/logs/pantavisor.log```. There is more information about cloud-backed logging at the end of this guide.

### Linux and Initrd

Both these entries serve an obvious purpose: they map the boot assets (initrd and kernel blobs) to bootloader-friendly locations depending on your bootloader's configuration. In this case 0base.cpio.xz is the initrd which contains Pantavisor and the demo helpers, and kernel.img is the RPi3 uImage for u-boot to consume. On device you can see these, and their mappings on:

```
$ telnet 192.168.53.X
# ls -i /storage/trails/0/.pv
     76 pv-initrd.img       75 pv-kernel.img
# ls -i /storage/trails/0
     72 0base.cpio.xz                      70 lede-17014-malta-be.squashfs       69 lede-lxc.conf                      79 pantavisor.json
     71 kernel.img                         78 lede-17014-os.json                 73 pantavisor.cpio.xz4
```

### Platforms

A Pantavisor platform is what defines how each container is setup on the running device. In this example there is only one platform **lede-17014-os**, and therefore it is the only full-system container which is brought up. This platform is further defined in the JSON file that has it's own name (lede-17014-os.json).

```
{
  "configs": [
    "lede-lxc.conf"
  ],
  "exec": "/sbin/init",
  "name": "lede-17014-os",
  "share": [
    "NETWORK",
    "UTS",
    "IPC"
  ],
  "type": "lxc"
}
```

This JSON represents the bits that Pantavisor needs to know to correctly bring up the container. The name is what is matched in **pantavisor.json** and the other fields are self-explanatory. We have a pointer to the ```init``` process of the container, the namespaces we would like to share from the root, the list of configs to pass to the runtime and ultimately, the type.

The type defines which container "runtime" we will use. In this current image there is only support for the lxc runtime, provided via the pv_lxc plugin. There is a very simple API through which more runtimes and be extended, but ultimately the plugin is in charge to interpret all the data above and do the correct setup on behalf of Pantavisor's controller.

### Volumes

The volumes list is a way to give arbitrary files on the checkout a mount point on the running system. In this case we ask Pantavisor to mount the set ```["lede-17014-malta-be.squashfs"]``` so it becomes available to the running platform.

Volumes are mounted onto the ```/volumes``` directory and on our example it looks like this:

```
$ telnet 192.168.53.X
# mount
...
/dev/loop0 on /volumes/lede-17014-malta-be.squashfs type squashfs (ro,relatime)
...
```

If you take a look at the platform configuration file ```lede-lxc.conf``` we can see it uses direct references to the mount point to perform it's own setup operations.

```
$ cat lede-lxc.conf
...
lxc.rootfs = overlayfs:/volumes/lede-17014-malta-be.squashfs:/storage/lede.disk/upper
...
```

### Firmware

In this example there is no separate firmware (because malta qemu requires no such thing) however the system also supports a firmware volume which can be defined in pantavisor.json. In a different device this would be packaged up as a squashfs and wrangled around by Pantavisor so the kernel can find the blobs in the right places.

### Driving your device with PVR

The whole point of these tools is the ability to quickly and safely iterating on changes to the platforms, volumes, and full device state with simple and familiar revisioning-system operations.

For example, let's consider our current running device. What if I could take an example share endpoint from somebody else, clone that state and then post it to my device in order to give it a try.

An example device has been setup at:

https://www.pantahub.com/u/examples/devices/5c53349edc5160000998ebe3

This reference device is the same one we used before in the build system, and the step revision "0" should have the same assets present on our current running image.

For demo purposes, the example device also has a step revision "1" which has a second container and more assets in place. We can for example clone this example step with PVR by simply doing:

```
$ pvr clone https://pvr.pantahub.com/examples/malta-qemu/1 example-lede-sensortag
*** Login (/type [R] to register) @ https://api.pantahub.com/auth (realm=pantahub services) ***
Username: ricmm
Password: *****
55aa090d0eb0722 [OK] Total: 940656 Bytes at 106856 Bytes/Sec
85686be7fa1ae51 [OK] Total: 2097152 Bytes at 323902 Bytes/Sec
8e52746afe70aa2 [OK] Total: 310 Bytes at 373 Bytes/Sec
b0bc9530b7812d3 [OK] Total: 5595136 Bytes at 270325 Bytes/Sec
9855f7533c9ff73 [OK] Total: 636 Bytes at 112 Bytes/Sec
5bf5463be0c6307 [OK] Total: 2089583 Bytes at 166173 Bytes/Sec
d6c10fc92841f60 [OK] Total: 1724416 Bytes at 136848 Bytes/Sec
ecaec2f8a4d19f4 [OK] Total: 540 Bytes at 48 Bytes/Sec
a6dc598a7306830 [OK] Total: 1048576 Bytes at 365025 Bytes/Sec
```

You can then do operations on this checked-out example under ```example-lede-sensortag```, like modify a config file or adapt something to your own needs. Or you could also just push it as-is to our running device. Just do, as if you were on a git tree and pushing to a different origin:

```
$ cd example-lede-sensortag/
$ pvr post https://api.pantahub.com/trails/59f197cb37f84b000d459fa3
writable-sen [OK]
0base.cpio.x [OK]
kernel.im [OK]
lede-17014-m [OK]
lede-lxc.con [OK]
sensortag-az [OK]
sensortag-az [OK]
writable.ext [OK]
Posted JSON: {"id":"59f197cb37f84b000d459fa3-1","owner":"prn:::accounts:/58dcbb476e2bc326666bfae2","device":"prn:::devices:/59f197cb37f84b000d459fa3","committer":"","trail-id":"59f197cb37f84b000d459fa3","rev":1,"commit-msg":"","state":{"#spec":"pantavisor-multi-platform@1","0base.cpio.xz":"55aa090d0eb072227405f293023f54add2fe2bf3065cd7663536f698f07c97df","README":"9855f7533c9ff732f6407d90a85990007e41f91c400b0dbdfd8e66e2e2476d6a","kernel.img":"5bf5463be0c6307adb3c9293b62b3ac160f0e13aa89c1544f614e8203fecf944","lede-17014-malta-be.squashfs":"d6c10fc92841f607de5327407b7f547e2c2623d2007f61c509b130530a0e67bf","lede-17014-os.json":{"configs":["lede-lxc.conf"],"exec":"/sbin/init","name":"lede-17014-os","share":["NETWORK","UTS","IPC"],"type":"lxc"},"lede-lxc.conf":"ecaec2f8a4d19f42b25cf2fab8ffdfdb2cb39fd54e5279500bc03673bfa292fc","pantavisor.json":{"initrd":"0base.cpio.xz","linux":"kernel.img","platforms:":["lede-17014-os","sensortag-azure"],"volumes":["lede-17014-malta-be.squashfs","writable.ext4","sensortag-azure-mips-be.squashfs","writable-sensortag-azure.ext4"]},"sensortag-azure-lxc.conf":"8e52746afe70aa23f73a51277aef8c40023c84680aacc241c67888bb576d6f8c","sensortag-azure-mips-be.squashfs":"b0bc9530b7812d3fc7381280e3415d37f1744d8f4060e1d920ea9475d9061e83","sensortag-azure.json":{"configs":["sensortag-azure-lxc.conf"],"exec":"/sbin/init","name":"sensortag-azure","share":["NETWORK","UTS","IPC"],"type":"lxc"},"writable-sensortag-azure.ext4":"a6dc598a7306830f34467cf9a6f4cdfbeb77398edfa9ba73bf9ee8a22b12a0c8","writable.ext4":"85686be7fa1ae515c1e3eb6fcc4065aa6bed1dfdcc6e9f46fdb2ba434d6c9a38"},"progress":{"progress":0,"status-msg":"","status":"NEW","log":""},"step-time":"2017-10-26T09:33:44.143648896Z","progress-time":"1970-01-01T00:00:00Z"}
```

Two important things to note here. First of all, we are using our user ("ricmm") to post to the device we own. We cloned with our user from a Public repository, and then we pushed the same step as-is to my own local device. Also we can see fully generated JSON of the step which is being pushed, including an automatic revision increment suggest by the cloud (in this case, revision 1).

This shows how you can easily share full checkouts between multiple devices, as well as put out "reference" states that can be consumed by other developers and tested out on their devices with a couple very simple operations. In this case we added a whole secondary platform, including a container rootfs. The change could also be a complete change of all containers, including the base LEDE one (for example to a whole different revision) -- even different kernels and a new initrd *!*

## Monitoring your device on PantaHub.com

### Device dashboard

PantaHub.com provides a fully fledged monitoring interface so that you can interact with your account's own device and object registry. This let's you visualize the current state of each device, how they are walking through available updates as well as doing introspection on the details of each step, including the objects that make it up.

![](ph-list.png)

You can also see your device details directly on PantaHub

![](ph-details.png)

### Cloud-logging

Pantavisor also exposes a cloud-backed logging feature. This feature is currently only used internally by Pantavisor to commit its internal logs every now and then to the cloud for introspection by the user, but it is also meant for injection of logs by running platforms, either automated or manual.

Logs are currently very aggressive on the default image configurations to put pressure on the backend and smoke out bugs, so the frequency of logs on the backend storage will be very high.

***

## Now... the rest is up to you!
