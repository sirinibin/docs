FROM pypy as builder

WORKDIR /work

RUN pip install mkdocs mkdocs-material
COPY . .
RUN mkdocs build

FROM nginx

COPY --from=builder /work/site /usr/share/nginx/html

